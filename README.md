# ArrayList源码分析

Fail-Fast机制: 是Java集合框架中结构发生改变的时候,快速失败的机制(错误检测机制)
 * 在发生add remove操作 modCount遍历都会自增1  在单线程或者多线程情况下都有可能出现快速失败机制
 * 场景1: 单线程情况下使用.iterator()方法进行迭代  中途突然删除或者新增一个元素就会报 Exception in thread "main" java.util.ConcurrentModificationException
 * 场景2: 多线程情况下使用.iterator()方法进行迭代和场景1是一样的
 * 场景1请见test1.java代码  场景2请见test2.java代码
 * 总结:
 * 只要使用迭代器对容器里面元素进行遍历,不管是单线程还是多线程情况下  只要你对元素进行add或者remove操作,都会出现快速失败机制
 * 它的底层是在开始迭代之前取出变量modCount值,然后使用一个局部变量接收,每次迭代的时候都会判断全局变量的modCount值如果不等于局部变量的modCount值就会抛出异常