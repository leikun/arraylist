package com.lk.arraylist;

/**
 * @ClassName List
 * @Description
 * @Author leikun
 * @Date2019/9/5 9:32
 **/
public interface List<E> {

    /**
     * 实际个数
     *
     * @return
     */
    int size();

    /**
     * 判断容器是否为空
     *
     * @return
     */
    boolean isEmpty();

    /**
     * 是否包含某个元素
     *
     * @param o
     * @return
     */
    boolean contains(Object o);

    /**
     * 新增元素
     *
     * @param e
     * @return
     */
    boolean add(E e);

    /**
     * 删除元素
     *
     * @param o
     * @return
     */
    boolean remove(Object o);

    /**
     * 清空容器
     */
    void clear();

    /**
     * 根据index查找元素
     *
     * @param index
     * @return
     */
    E get(int index);

    /**
     * 根据index 设置新值
     *
     * @param index
     * @param element
     * @return
     */
    E set(int index, E element);

    /**
     * 根据下标新增元素
     *
     * @param index
     * @param element
     */
    void add(int index, E element);

    /**
     * 根据下标删除元素
     *
     * @param index
     * @return
     */
    E remove(int index);
}
