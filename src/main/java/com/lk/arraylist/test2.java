package com.lk.arraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName test2
 * @Description  多线程情况下出现fail-fast情况
 * @Author leikun
 * @Date2019/9/5 15:35
 **/
public class test2 {

    static List<Integer> list = new ArrayList<>();

    public static void print() {
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void main(String[] args) {
        new Thread(() -> {
            for(int i=0;i<100;i++){
                list.add(i+1);
                print();
            }
        }).start();
        new Thread(() -> {
            for(int i=0;i<100;i++){
                list.add(i+1);
                print();
            }
        }).start();
    }
}
