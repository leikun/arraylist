package com.lk.arraylist;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName test
 * @Description 单线程情况下出现fail-fast情况
 * @Author leikun
 * @Date2019/9/5 15:14
 **/
public class test1 {
    public static void main(String[] args) {
        List<Integer> nums = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            nums.add(i + 1);
        }
        Iterator<Integer> iterator = nums.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Integer value = iterator.next();
            System.out.println(value);
            if (i++ == 10) {
                nums.remove(10);
            }
        }

    }
}
